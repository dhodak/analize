﻿namespace WindowsFormsApp5
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.buttonletter = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.buttonword = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.labeltry = new System.Windows.Forms.Label();
            this.labelnumber = new System.Windows.Forms.Label();
            this.labeldisplay = new System.Windows.Forms.Label();
            this.buttonstart = new System.Windows.Forms.Button();
            this.labelsadrzi = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(17, 434);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(10, 10);
            this.panel1.TabIndex = 1;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 16;
            this.listBox1.Location = new System.Drawing.Point(17, 280);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(211, 116);
            this.listBox1.TabIndex = 2;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // buttonletter
            // 
            this.buttonletter.Location = new System.Drawing.Point(17, 173);
            this.buttonletter.Name = "buttonletter";
            this.buttonletter.Size = new System.Drawing.Size(114, 38);
            this.buttonletter.TabIndex = 3;
            this.buttonletter.Text = "Letter";
            this.buttonletter.UseVisualStyleBackColor = true;
            this.buttonletter.Click += new System.EventHandler(this.buttonletter_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(137, 181);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(48, 22);
            this.textBox1.TabIndex = 4;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // buttonword
            // 
            this.buttonword.Location = new System.Drawing.Point(218, 173);
            this.buttonword.Name = "buttonword";
            this.buttonword.Size = new System.Drawing.Size(102, 35);
            this.buttonword.TabIndex = 5;
            this.buttonword.Text = "Word";
            this.buttonword.UseVisualStyleBackColor = true;
            this.buttonword.Click += new System.EventHandler(this.buttonword_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(326, 181);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(48, 22);
            this.textBox2.TabIndex = 6;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // labeltry
            // 
            this.labeltry.AutoSize = true;
            this.labeltry.Location = new System.Drawing.Point(13, 128);
            this.labeltry.Name = "labeltry";
            this.labeltry.Size = new System.Drawing.Size(105, 17);
            this.labeltry.TabIndex = 7;
            this.labeltry.Text = "Number of tries";
            // 
            // labelnumber
            // 
            this.labelnumber.AutoSize = true;
            this.labelnumber.Location = new System.Drawing.Point(124, 128);
            this.labelnumber.Name = "labelnumber";
            this.labelnumber.Size = new System.Drawing.Size(16, 17);
            this.labelnumber.TabIndex = 8;
            this.labelnumber.Text = "5";
            this.labelnumber.Click += new System.EventHandler(this.labelnumber_Click);
            // 
            // labeldisplay
            // 
            this.labeldisplay.AutoSize = true;
            this.labeldisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labeldisplay.Location = new System.Drawing.Point(38, 42);
            this.labeldisplay.Name = "labeldisplay";
            this.labeldisplay.Size = new System.Drawing.Size(0, 39);
            this.labeldisplay.TabIndex = 9;
            this.labeldisplay.Click += new System.EventHandler(this.labeldisplay_Click);
            // 
            // buttonstart
            // 
            this.buttonstart.Location = new System.Drawing.Point(571, 280);
            this.buttonstart.Name = "buttonstart";
            this.buttonstart.Size = new System.Drawing.Size(231, 163);
            this.buttonstart.TabIndex = 10;
            this.buttonstart.Text = "Guess";
            this.buttonstart.UseVisualStyleBackColor = true;
            this.buttonstart.Click += new System.EventHandler(this.buttonstart_Click);
            // 
            // labelsadrzi
            // 
            this.labelsadrzi.AutoSize = true;
            this.labelsadrzi.Location = new System.Drawing.Point(24, 234);
            this.labelsadrzi.Name = "labelsadrzi";
            this.labelsadrzi.Size = new System.Drawing.Size(0, 17);
            this.labelsadrzi.TabIndex = 11;
            this.labelsadrzi.Click += new System.EventHandler(this.labelsadrzi_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(825, 479);
            this.Controls.Add(this.labelsadrzi);
            this.Controls.Add(this.buttonstart);
            this.Controls.Add(this.labeldisplay);
            this.Controls.Add(this.labelnumber);
            this.Controls.Add(this.labeltry);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.buttonword);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.buttonletter);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Hangman";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button buttonletter;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button buttonword;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label labeltry;
        private System.Windows.Forms.Label labelnumber;
        private System.Windows.Forms.Label labeldisplay;
        private System.Windows.Forms.Button buttonstart;
        private System.Windows.Forms.Label labelsadrzi;
    }
}

