﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp5
{
    
    public partial class Form1 : Form
    {
        class Word
        {
            #region data_members
            private string name;

            #endregion
            #region public_methods
            public Word()
            {
                name = "word";
            }
            public Word(string n)
            {
                name = n;
            }
            public override string ToString()
            {
                return name;
            }
            #endregion
        }
        //kreiranje liste rijeci
        List<Word> listwords = new List<Word>();
        string path = "C:\\data.txt";
        int m;
       

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {
                string line;
                while ((line = reader.ReadLine()) != null) // čitanje svih linija iz dat.
                {
                    string[] parts = line.Split('\t');
                    Word W = new Word(parts[0]); // novi objekt
                    listwords.Add(W); // umetanje objekta u listu
                }
                listBox1.DataSource = null;
                listBox1.DataSource = listwords;
            }

            
        }
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void labelnumber_Click(object sender, EventArgs e)
        {

        }

        private void buttonletter_Click(object sender, EventArgs e)
        {   //gumb za provjeru da li rijec sadrzi upisano slovo
            //smanjivanje zivota ukoliko ne sadrzi
            int number, temp;
            string b = textBox1.Text;
            string a = listBox1.Items[m].ToString();
            if (textBox1.Text == "")
                MessageBox.Show("Box is empty");
            else {
                if (a.Contains(b))
                    labelsadrzi.Text = "It contains that letter!";
                else
                {
                    number = Int32.Parse(labelnumber.Text);
                    if (number <= 0)
                    {
                        MessageBox.Show("Hanged");
                        labelnumber.Text = "0";

                    }
                    temp = number - 1;


                    labelnumber.Text = temp.ToString();
                    if (temp <= 0 || temp == -1)
                    {
                        MessageBox.Show("Hanged");
                        labelnumber.Text = "0";
                    }

                }    
            }



        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttonword_Click(object sender, EventArgs e)
        {
            //gumb za provjeru da li je to rijec koja se trazi
            //smanjivanje zivota ukoliko nije
            int number, temp;
            string b = textBox2.Text;
            string a = listBox1.Items[m].ToString();
            if (textBox2.Text == "")
                MessageBox.Show("Box is empty");
            else
            {

                if (a.Contains(b))
                    MessageBox.Show("You win!");
                else
                {
                    number = Int32.Parse(labelnumber.Text);
                    if (number <= 0)
                    {
                        MessageBox.Show("Hanged");
                        labelnumber.Text = "0";

                    }
                    temp = number - 1;


                    labelnumber.Text = temp.ToString();
                    if (temp <= 0 || temp == -1)
                    {
                        MessageBox.Show("Hanged");
                        labelnumber.Text = "0";
                    }
                }
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void labeldisplay_Click(object sender, EventArgs e)
        {
            
        }

        private void buttonstart_Click(object sender, EventArgs e)
        {
            //nasumican odabir rijeci iz liste
            Random random = new Random();
            int a = random.Next(0, listBox1.Items.Count);
            string b = listBox1.Items[a].ToString();
            int c = b.Length-1;
            string d = "_ ";
            string result = string.Concat(Enumerable.Repeat(d, c));
            labeldisplay.Text = b.ElementAt(0).ToString()+result;
            m = a;
            labelnumber.Text = "5";
        }

        private void labelsadrzi_Click(object sender, EventArgs e)
        {

        }
    }

}