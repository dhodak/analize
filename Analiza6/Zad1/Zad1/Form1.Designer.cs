﻿namespace Zad1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.operandBox1 = new System.Windows.Forms.TextBox();
            this.Operand2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.buttonplus = new System.Windows.Forms.Button();
            this.buttonminus = new System.Windows.Forms.Button();
            this.buttonmnozenje = new System.Windows.Forms.Button();
            this.buttondijeljenje = new System.Windows.Forms.Button();
            this.buttonsin = new System.Windows.Forms.Button();
            this.buttoncos = new System.Windows.Forms.Button();
            this.buttonkorijen = new System.Windows.Forms.Button();
            this.buttonpotencija = new System.Windows.Forms.Button();
            this.buttonlog = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.rezultat = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(46, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Operand 1";
            // 
            // operandBox1
            // 
            this.operandBox1.Location = new System.Drawing.Point(128, 35);
            this.operandBox1.Name = "operandBox1";
            this.operandBox1.Size = new System.Drawing.Size(121, 22);
            this.operandBox1.TabIndex = 1;
            this.operandBox1.TextChanged += new System.EventHandler(this.operandBox1_TextChanged);
            // 
            // Operand2
            // 
            this.Operand2.AutoSize = true;
            this.Operand2.Location = new System.Drawing.Point(46, 97);
            this.Operand2.Name = "Operand2";
            this.Operand2.Size = new System.Drawing.Size(76, 17);
            this.Operand2.TabIndex = 2;
            this.Operand2.Text = "Operand 2";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(128, 94);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(121, 22);
            this.textBox1.TabIndex = 3;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // buttonplus
            // 
            this.buttonplus.Location = new System.Drawing.Point(358, 31);
            this.buttonplus.Name = "buttonplus";
            this.buttonplus.Size = new System.Drawing.Size(54, 35);
            this.buttonplus.TabIndex = 4;
            this.buttonplus.Text = "+";
            this.buttonplus.UseVisualStyleBackColor = true;
            this.buttonplus.Click += new System.EventHandler(this.buttonplus_Click);
            // 
            // buttonminus
            // 
            this.buttonminus.Location = new System.Drawing.Point(442, 31);
            this.buttonminus.Name = "buttonminus";
            this.buttonminus.Size = new System.Drawing.Size(51, 35);
            this.buttonminus.TabIndex = 5;
            this.buttonminus.Text = "-";
            this.buttonminus.UseVisualStyleBackColor = true;
            this.buttonminus.Click += new System.EventHandler(this.buttonminus_Click);
            // 
            // buttonmnozenje
            // 
            this.buttonmnozenje.Location = new System.Drawing.Point(358, 88);
            this.buttonmnozenje.Name = "buttonmnozenje";
            this.buttonmnozenje.Size = new System.Drawing.Size(50, 35);
            this.buttonmnozenje.TabIndex = 6;
            this.buttonmnozenje.Text = "*";
            this.buttonmnozenje.UseVisualStyleBackColor = true;
            this.buttonmnozenje.Click += new System.EventHandler(this.buttonmnozenje_Click);
            // 
            // buttondijeljenje
            // 
            this.buttondijeljenje.Location = new System.Drawing.Point(442, 88);
            this.buttondijeljenje.Name = "buttondijeljenje";
            this.buttondijeljenje.Size = new System.Drawing.Size(51, 35);
            this.buttondijeljenje.TabIndex = 7;
            this.buttondijeljenje.Text = "/";
            this.buttondijeljenje.UseVisualStyleBackColor = true;
            this.buttondijeljenje.Click += new System.EventHandler(this.buttondijeljenje_Click);
            // 
            // buttonsin
            // 
            this.buttonsin.Location = new System.Drawing.Point(523, 31);
            this.buttonsin.Name = "buttonsin";
            this.buttonsin.Size = new System.Drawing.Size(54, 35);
            this.buttonsin.TabIndex = 8;
            this.buttonsin.Text = "sin";
            this.buttonsin.UseVisualStyleBackColor = true;
            this.buttonsin.Click += new System.EventHandler(this.buttonsin_Click);
            // 
            // buttoncos
            // 
            this.buttoncos.Location = new System.Drawing.Point(523, 88);
            this.buttoncos.Name = "buttoncos";
            this.buttoncos.Size = new System.Drawing.Size(53, 34);
            this.buttoncos.TabIndex = 9;
            this.buttoncos.Text = "cos";
            this.buttoncos.UseVisualStyleBackColor = true;
            this.buttoncos.Click += new System.EventHandler(this.buttoncos_Click);
            // 
            // buttonkorijen
            // 
            this.buttonkorijen.Location = new System.Drawing.Point(359, 142);
            this.buttonkorijen.Name = "buttonkorijen";
            this.buttonkorijen.Size = new System.Drawing.Size(48, 35);
            this.buttonkorijen.TabIndex = 10;
            this.buttonkorijen.Text = "sqrt";
            this.buttonkorijen.UseVisualStyleBackColor = true;
            this.buttonkorijen.Click += new System.EventHandler(this.buttonkorijen_Click);
            // 
            // buttonpotencija
            // 
            this.buttonpotencija.Location = new System.Drawing.Point(441, 142);
            this.buttonpotencija.Name = "buttonpotencija";
            this.buttonpotencija.Size = new System.Drawing.Size(51, 35);
            this.buttonpotencija.TabIndex = 11;
            this.buttonpotencija.Text = "n^2";
            this.buttonpotencija.UseVisualStyleBackColor = true;
            this.buttonpotencija.Click += new System.EventHandler(this.buttonpotencija_Click);
            // 
            // buttonlog
            // 
            this.buttonlog.Location = new System.Drawing.Point(524, 144);
            this.buttonlog.Name = "buttonlog";
            this.buttonlog.Size = new System.Drawing.Size(52, 33);
            this.buttonlog.TabIndex = 12;
            this.buttonlog.Text = "log";
            this.buttonlog.UseVisualStyleBackColor = true;
            this.buttonlog.Click += new System.EventHandler(this.buttonlog_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(63, 245);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 17);
            this.label2.TabIndex = 13;
            this.label2.Text = "Rezultat:";
            // 
            // rezultat
            // 
            this.rezultat.AutoSize = true;
            this.rezultat.Location = new System.Drawing.Point(133, 245);
            this.rezultat.Name = "rezultat";
            this.rezultat.Size = new System.Drawing.Size(16, 17);
            this.rezultat.TabIndex = 14;
            this.rezultat.Text = "0";
            this.rezultat.Click += new System.EventHandler(this.rezultat_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.ClientSize = new System.Drawing.Size(591, 379);
            this.Controls.Add(this.rezultat);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buttonlog);
            this.Controls.Add(this.buttonpotencija);
            this.Controls.Add(this.buttonkorijen);
            this.Controls.Add(this.buttoncos);
            this.Controls.Add(this.buttonsin);
            this.Controls.Add(this.buttondijeljenje);
            this.Controls.Add(this.buttonmnozenje);
            this.Controls.Add(this.buttonminus);
            this.Controls.Add(this.buttonplus);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.Operand2);
            this.Controls.Add(this.operandBox1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Form1";
            this.Text = "Kalkulator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox operandBox1;
        private System.Windows.Forms.Label Operand2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button buttonplus;
        private System.Windows.Forms.Button buttonminus;
        private System.Windows.Forms.Button buttonmnozenje;
        private System.Windows.Forms.Button buttondijeljenje;
        private System.Windows.Forms.Button buttonsin;
        private System.Windows.Forms.Button buttoncos;
        private System.Windows.Forms.Button buttonkorijen;
        private System.Windows.Forms.Button buttonpotencija;
        private System.Windows.Forms.Button buttonlog;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label rezultat;
    }
}

