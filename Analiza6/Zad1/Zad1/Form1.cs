﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Zad1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void operandBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttonplus_Click(object sender, EventArgs e) //zbrajanje
        {      double op1, op2;
            if (!double.TryParse(operandBox1.Text, out op1))
                MessageBox.Show("Pogresan unos operanda 1!", "Pogreska!");
            else if (!double.TryParse(textBox1.Text, out op2))
                MessageBox.Show("Pogresan unos operanda 2!", "Pogreska!");
            else
            {
                rezultat.Text = (op1 + op2).ToString();
            }

        }

        private void buttonminus_Click(object sender, EventArgs e)//oduzimanje
        {
            double op1, op2;
            if (!double.TryParse(operandBox1.Text, out op1))
                MessageBox.Show("Pogresan unos operanda 1!", "Pogreska!");
            else if (!double.TryParse(textBox1.Text, out op2))
                MessageBox.Show("Pogresan unos operanda 2!", "Pogreska!");
            else
            {
                rezultat.Text = (op1 - op2).ToString();
            }
        }

        private void buttonsin_Click(object sender, EventArgs e)//sinus
        {
            double op1, op2;
            if (!double.TryParse(operandBox1.Text, out op1) && !double.TryParse(textBox1.Text, out op2))
                MessageBox.Show("Pogresan unos operanda!", "Pogreska!");
            else if (double.TryParse(textBox1.Text, out op2) && double.TryParse(operandBox1.Text, out op1))
            {
                MessageBox.Show("Unijeti samo jedan operand!");
            } //dodatne provjere ukoliko se unese samo u operand 1 ili samo u operand 2
            else if(!double.TryParse(textBox1.Text, out op2) || double.TryParse(operandBox1.Text, out op1))
            {
                rezultat.Text = (Math.Sin(op1)).ToString();
            }
            else if(double.TryParse(textBox1.Text, out op2) || !double.TryParse(operandBox1.Text, out op1))
            {
                rezultat.Text = (Math.Sin(op2)).ToString();
            }
            
        }

        private void buttonmnozenje_Click(object sender, EventArgs e)//mnozenje
        {
            double op1, op2;
            if (!double.TryParse(operandBox1.Text, out op1))
                MessageBox.Show("Pogresan unos operanda 1!", "Pogreska!");
            else if (!double.TryParse(textBox1.Text, out op2))
                MessageBox.Show("Pogresan unos operanda 2!", "Pogreska!");
            else
            {
                rezultat.Text = (op1 * op2).ToString();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void buttonkorijen_Click(object sender, EventArgs e)//korijen
        {
            double op1, op2;
            if (!double.TryParse(operandBox1.Text, out op1) && !double.TryParse(textBox1.Text, out op2))
                MessageBox.Show("Pogresan unos operanda!", "Pogreska!");
            else if (double.TryParse(textBox1.Text, out op2) && double.TryParse(operandBox1.Text, out op1))
            {
                MessageBox.Show("Unijeti samo jedan operand!");
            }//dodatne provjere ukoliko se unese samo u operand 1 ili samo u operand 2
            else if (!double.TryParse(textBox1.Text, out op2) || double.TryParse(operandBox1.Text, out op1))
            {
                rezultat.Text = (Math.Sqrt(op1)).ToString();
            }
            else if (double.TryParse(textBox1.Text, out op2) || !double.TryParse(operandBox1.Text, out op1))
            {
                rezultat.Text = (Math.Sqrt(op2)).ToString();
            }
        }

        private void buttonpotencija_Click(object sender, EventArgs e)//potenciranje na drugu
        {
            double op1, op2;
            if (!double.TryParse(operandBox1.Text, out op1) && !double.TryParse(textBox1.Text, out op2))
                MessageBox.Show("Pogresan unos operanda!", "Pogreska!");
            else if (double.TryParse(textBox1.Text, out op2) && double.TryParse(operandBox1.Text, out op1))
            {
                MessageBox.Show("Unijeti samo jedan operand!");
            }//dodatne provjere ukoliko se unese samo u operand 1 ili samo u operand 2
            else if (!double.TryParse(textBox1.Text, out op2) || double.TryParse(operandBox1.Text, out op1))
            {
                rezultat.Text = (Math.Pow((op1),2)).ToString();
            }
            else if (double.TryParse(textBox1.Text, out op2) || !double.TryParse(operandBox1.Text, out op1))
            {
                rezultat.Text = (Math.Pow((op2),2)).ToString();
            }
        }

        private void buttonlog_Click(object sender, EventArgs e)//logaritam po bazi 10
        {
            double op1, op2;
            if (!double.TryParse(operandBox1.Text, out op1) && !double.TryParse(textBox1.Text, out op2))
                MessageBox.Show("Pogresan unos operanda!", "Pogreska!");
            else if (double.TryParse(textBox1.Text, out op2) && double.TryParse(operandBox1.Text, out op1))
            {
                MessageBox.Show("Unijeti samo jedan operand!");
            }//dodatne provjere ukoliko se unese samo u operand 1 ili samo u operand 2
            else if (!double.TryParse(textBox1.Text, out op2) || double.TryParse(operandBox1.Text, out op1))
            {
                rezultat.Text = (Math.Log10(op1)).ToString();
            }
            else if (double.TryParse(textBox1.Text, out op2) || !double.TryParse(operandBox1.Text, out op1))
            {
                rezultat.Text = (Math.Log10(op2)).ToString();
            }
        }

        private void buttondijeljenje_Click(object sender, EventArgs e)//dijeljenje
        {
            double op1, op2;
            if (!double.TryParse(operandBox1.Text, out op1))
                MessageBox.Show("Pogresan unos operanda 1!", "Pogreska!");
            else if (!double.TryParse(textBox1.Text, out op2))
                MessageBox.Show("Pogresan unos operanda 2!", "Pogreska!");
            else
            {
                rezultat.Text = (op1 / op2).ToString();
            }
        }

        private void buttoncos_Click(object sender, EventArgs e)//kosinus
        {
            double op1, op2;
            if (!double.TryParse(operandBox1.Text, out op1) && !double.TryParse(textBox1.Text, out op2))
                MessageBox.Show("Pogresan unos operanda!", "Pogreska!");
            else if (double.TryParse(textBox1.Text, out op2) && double.TryParse(operandBox1.Text, out op1))
            {
                MessageBox.Show("Unijeti samo jedan operand!");
            }//dodatne provjere ukoliko se unese samo u operand 1 ili samo u operand 2
            else if (!double.TryParse(textBox1.Text, out op2) || double.TryParse(operandBox1.Text, out op1))
            {
                rezultat.Text = (Math.Cos(op1)).ToString();
            }
            else if (double.TryParse(textBox1.Text, out op2) || !double.TryParse(operandBox1.Text, out op1))
            {
                rezultat.Text = (Math.Cos(op2)).ToString();
            }
        }

        private void rezultat_Click(object sender, EventArgs e)
        {

        }
    }
}
