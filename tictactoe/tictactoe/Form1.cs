﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tictactoe
{
    public partial class Form1 : Form
    {
        bool turn = false;
        int turn_count = 0;
        static String player1, player2;
        public Form1()
        {
            InitializeComponent();
        }
        public static void playername(String name1, String name2)
        {
            player1 = name1;
            player2 = name2;
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("A simple tic tac toe game made for science!\n" +
                "Autor: David Hodak");
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button_click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (turn)
                b.Text = "O";
            else
                b.Text = "X";
            turn = !turn;
            b.Enabled = false;

            checkwinner();
        }
        private void checkwinner()
        {
            turn_count++;
            bool winner = false;
            //horizontal
            if ((a1.Text == a2.Text) && (a2.Text == a3.Text) && (!a1.Enabled))
                winner = true;
            else if ((b1.Text == b2.Text) && (b2.Text == b3.Text) && (!b1.Enabled))
                winner = true;
            else if((c1.Text == c2.Text) && (c2.Text == c3.Text)&&(!c1.Enabled))
                winner = true;

            //vertical
            if ((a1.Text == b1.Text) && (b1.Text == c1.Text) && (!a1.Enabled))
                winner = true;
            else if ((a2.Text == b2.Text) && (b2.Text == c2.Text) && (!a2.Enabled))
                winner = true;
            else if ((a3.Text == b3.Text) && (b3.Text == c3.Text) && (!a3.Enabled))
                winner = true;
            //diagnol
            if ((a1.Text == b2.Text) && (b2.Text == c3.Text) && (!a1.Enabled))
                winner = true;
            else if ((a3.Text == b2.Text) && (b2.Text == c1.Text) && (!a3.Enabled))
                winner = true;
            

            if (winner)
            {
                disablebuttons();//if there is winner disable the rest of buttons

                string player = "";
                if (turn)
                {
                    player = player1;
                    xwincount.Text = (Int32.Parse(xwincount.Text) + 1).ToString();
                }
                else
                {
                    player = player2;
                    owinscount.Text = (Int32.Parse(owinscount.Text) + 1).ToString();
                }
                
                MessageBox.Show(player+" is winner!");
            }
            else
            {
                if (turn_count == 9)
                {
                    drawcount.Text=(Int32.Parse(drawcount.Text) + 1).ToString();
                    MessageBox.Show("Draw!");

                }

            }

        }//endcheckwinner

        private void disablebuttons()
        {
             //try catch because we want eliminate file and help as buttons, so they are not disabled
            
                foreach (Control c in Controls)
                {
                try
                {
                    Button b = (Button)c;
                    b.Enabled = false;
                }
                catch { }
                }
            
            

        }

        private void newGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            turn = true;
            turn_count = 0;

            
                foreach (Control c in Controls)
                {
                try {
                    Button b = (Button)c;
                    b.Enabled = true;
                    b.Text = " ";
                }
                catch { }
                    
                }
            }
            
        

        private void button_enter(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (b.Enabled)
            {
                if (turn)
                {
                    b.Text = "O";
                }
                else
                    b.Text = "X";
            }
        }

        private void button_leave(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (b.Enabled)
            {
                b.Text = " ";
            }

        }

        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            owinscount.Text = "0";
            xwincount.Text = "0";
            drawcount.Text = "0";
        }

        private void resetPlayersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            
            form2.ShowDialog();
            
            label1.Text = player1;
            label3.Text = player2;
            owinscount.Text = "0";
            xwincount.Text = "0";
            drawcount.Text = "0";
            turn = true;
            turn_count = 0;


            foreach (Control c in Controls)
            {
                try
                {
                    Button b = (Button)c;
                    b.Enabled = true;
                    b.Text = " ";
                }
                catch { }

            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.ShowDialog();
            label1.Text = player1;
            label3.Text = player2;
        }
    }
}
